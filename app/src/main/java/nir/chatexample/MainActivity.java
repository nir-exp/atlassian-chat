package nir.chatexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import nir.chatexample.parsers.MessageEmoticonParser;
import nir.chatexample.parsers.MessageLinkParser;
import nir.chatexample.parsers.MessageMentionParser;

public class MainActivity extends AppCompatActivity {

	private View sendButton;
	private RecyclerView recyclerView;
	private EditText editText;
	private SimpleAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		sendButton = findViewById(R.id.chat_send_button);
		recyclerView = (RecyclerView) findViewById(R.id.chat_recycler_view);
		editText = (EditText) findViewById(R.id.chat_edit_text);

		LinearLayoutManager manager = new LinearLayoutManager(this);
		manager.setStackFromEnd(true);
		recyclerView.setLayoutManager(manager);
		adapter = new SimpleAdapter();
		recyclerView.setAdapter(adapter);

		// Build the message handler and assign to it the wanted parsers
		final MessageHandler messageHandler = new MessageHandler();
		messageHandler.addParser(new MessageLinkParser());
		messageHandler.addParser(new MessageEmoticonParser());
		messageHandler.addParser(new MessageMentionParser());

		sendButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String text = editText.getText().toString().trim();
				if (!text.isEmpty()) {
					adapter.addItem(getString(R.string.user_prefix) + text);

					editText.setEnabled(false);
					// On every user input, parse the message and send it to the adapter
					messageHandler.parse(text, new MessageHandler.OnMessageHandlerResultReady() {
						@Override
						public void onMessageHandlerResultReady(MessageHandler.MessageHandlerResult messageHandlerResult) {
							String reply = messageHandlerResult.getMessageMetaData();
							adapter.addItem(reply);
							recyclerView.scrollToPosition(adapter.getItemCount() - 1);
							editText.setEnabled(true);
						}

						@Override
						public void failure(Exception e) {
							Toast.makeText(MainActivity.this, "Parsing failed " + e.getMessage(), Toast.LENGTH_LONG).show();
							editText.setEnabled(true);
						}
					});

					editText.setText("");
					recyclerView.scrollToPosition(adapter.getItemCount() - 1);
				}
			}
		});

		// Example buttons
		findViewById(R.id.example_one_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				editText.setText("@chris you around?");
				sendButton.performClick();
			}
		});

		findViewById(R.id.example_two_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				editText.setText("Good morning! (megusta) (coffee)");
				sendButton.performClick();
			}
		});

		findViewById(R.id.example_three_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				editText.setText("Olympics are starting soon; http://www.nbcolympics.com");
				sendButton.performClick();
			}
		});

		findViewById(R.id.example_four_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				editText.setText("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016");
				sendButton.performClick();
			}
		});
	}
}

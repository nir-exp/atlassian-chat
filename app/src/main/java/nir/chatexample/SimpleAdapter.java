package nir.chatexample;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 18/05/2016.
 */
public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.SimpleViewHolder> {

	private List<String> items;

	public SimpleAdapter() {
		items = new ArrayList<>();
	}

	@Override
	public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new SimpleViewHolder(View.inflate(parent.getContext(), R.layout.text_item, null));
	}

	@Override
	public void onBindViewHolder(SimpleViewHolder holder, int position) {
		((TextView) holder.itemView).setText(items.get(position));
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	public void addItem(String str) {
		items.add(str);
		notifyItemInserted(items.size() - 1);
	}

	static public class SimpleViewHolder extends RecyclerView.ViewHolder {

		public SimpleViewHolder(View itemView) {
			super(itemView);
		}
	}
}



package nir.chatexample;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import nir.chatexample.parsers.MessageParser;
import nir.chatexample.parsers.ParserResult;

/**
 * This class holds a list of parsers and know how to operate them together
 */
public class MessageHandler {

	private final List<MessageParser> parsers;

	public MessageHandler() {
		this.parsers = new ArrayList<>();
	}

	public MessageHandler addParser(MessageParser messageParser) {
		parsers.add(messageParser);
		return this;
	}

	public MessageHandlerResult parse(String message, final OnMessageHandlerResultReady callback) {
		final MessageHandlerResult result = new MessageHandlerResult();

		result.setNewMessage(message);
		// This will be the final json to present to the user
		final JSONObject messageMetaData = new JSONObject();

		// Since the JSONs are handled async we need to count it to know when we are ready to deliver the result
		final AtomicInteger countDownParsers = new AtomicInteger(parsers.size());

		for (MessageParser messageParser : parsers) {
			ParserResult parseResult = messageParser.parse(result.getNewMessage());
			// If we want to change the user message, this will keep all of the changes
			String newMessage = parseResult.getMessageAfterReplacements() == null ? null : parseResult.getMessageAfterReplacements().toString();

			if (newMessage != null) {
				result.setNewMessage(newMessage);
			}

			messageParser.toJSON(parseResult, messageMetaData, new MessageParser.ToJSONCallback() {

				@Override
				public void success(JSONObject jsonObject) {
					if (countDownParsers.decrementAndGet() == 0) {
						// JSONObject toString escape the "/"
						result.setMessageMetaData(messageMetaData.toString().replace("\\/", "/"));
						callback.onMessageHandlerResultReady(result);
					}
				}

				@Override
				public void failure(Exception e) {
					callback.failure(e);
				}
			});
		}
		return result;
	}

	public interface OnMessageHandlerResultReady {
		void onMessageHandlerResultReady(MessageHandlerResult messageHandlerResult);

		void failure(Exception e);
	}

	public static class MessageHandlerResult {
		public String messageMetaData;
		public String newMessage;

		public MessageHandlerResult() {

		}

		public String getMessageMetaData() {
			return messageMetaData;
		}

		public void setMessageMetaData(String messageMetaData) {
			this.messageMetaData = messageMetaData;
		}

		public String getNewMessage() {
			return newMessage;
		}

		public void setNewMessage(String newMessage) {
			this.newMessage = newMessage;
		}
	}
}

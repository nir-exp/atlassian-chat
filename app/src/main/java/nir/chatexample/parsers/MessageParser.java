package nir.chatexample.parsers;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class MessageParser {

	// Thread pool for background works
	private final static Executor cachedExec = Executors.newCachedThreadPool();

	final protected Pattern pattern;

	public MessageParser() {
		pattern = compilePattern();
	}

	abstract protected String getPattern();

	/**
	 * Turn a set of matches result into a nice looking json
	 */
	@WorkerThread
	abstract protected JSONObject toJSONImpl(ParserResult parserResult, JSONObject jsonObject) throws Exception;

	public void toJSON(final ParserResult parserResult, final JSONObject jsonObject, @NonNull final ToJSONCallback toJSONCallback) {
		// For best practice, the callback will be replied in the calling thread
		final Handler callbackThreadHandler = new Handler();

		cachedExec.execute(new Runnable() {
			@Override
			public void run() {
				if (parserResult == null || parserResult.getMatches() == null || parserResult.getMatches().isEmpty()) {
					// An empty result set
					callbackThreadHandler.post(new Runnable() {
						@Override
						public void run() {
							toJSONCallback.success(jsonObject);
						}
					});
				} else {
					try {
						// This must be run in a worker thread
						final JSONObject result = toJSONImpl(parserResult, jsonObject);
						callbackThreadHandler.post(new Runnable() {
							@Override
							public void run() {
								toJSONCallback.success(result);
							}
						});
					} catch (final Exception e) {
						callbackThreadHandler.post(new Runnable() {
							@Override
							public void run() {
								toJSONCallback.failure(e);
							}
						});
					}
				}
			}
		});
	}

	/**
	 * A hook to do something when a match is found, override it for additional logic
	 */
	protected void onMatchFound(Matcher matcher, String match, ParserResult parserResult) {
	}

	protected Pattern compilePattern() {
		return Pattern.compile(getPattern());
	}

	/**
	 * Iterate over the matches, parse it and building a result with all of the matching groups
	 *
	 * @param msg - The message to parse
	 * @return a ParserResult object containing the data about this parsed message
	 */
	public ParserResult parse(String msg) {
		ParserResult result = new ParserResult();
		Matcher matcher = pattern.matcher(msg);
		ArrayList<String> matches = new ArrayList<>();

		while (matcher.find()) {
			String match = matcher.group();
			matches.add(cleanMatch(match));
			onMatchFound(matcher, match, result);
		}

		onAllMatchesFound(matcher, result);
		result.setMatches(matches);
		return result;
	}

	/**
	 * If the user chose to change the matches, this will append the rest of the sentence, it is also a
	 * good place to hook if more logic is needed
	 */
	protected void onAllMatchesFound(Matcher matcher, ParserResult result) {
		if (result.getMessageAfterReplacements() != null) {
			matcher.appendTail(result.getMessageAfterReplacements());
		}
	}

	/**
	 * We might catch more than we want to show, for example we catch "@Jhon", and want to show "Jhon"
	 * @param match
	 * @return
	 */
	protected String cleanMatch(String match) {
		return match;
	}

	/**
	 * An example of how I would add logic to this design
	 * this method will replace the match using a matchReplacer
	 * examples it can be useful for:
	 * a) if we have a special pattern for emojis like (happy), we can replace it with the real emoji
	 * b) if we want to priority some parsers over the others, if we have "hey @john, this is my email - an@email.com", we would like to
	 * first run a MessageEmailParser to make it "hey @john, this is my email - <an@email.com>" and afterwards the MessageMentionParser
	 * so it won't think that @email is a mention (The current implementation of MessageMentionParser will need to change to avoid mentions
	 * inside <> in this example).
	 */
	protected void replaceMatches(Matcher matcher, String match, ParserResult parserResult, @NonNull MatchReplacer matchReplacer) {
		if (parserResult.getMessageAfterReplacements() == null) {
			parserResult.setMessageAfterReplacements(new StringBuffer());
		}

		matcher.appendReplacement(parserResult.getMessageAfterReplacements(), matchReplacer.replaceMatch(match));
	}

	public interface MatchReplacer {
		String replaceMatch(String match);
	}

	public interface ToJSONCallback {
		void success(JSONObject jsonObject);

		void failure(Exception e);
	}
}

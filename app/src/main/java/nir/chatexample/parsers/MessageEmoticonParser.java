package nir.chatexample.parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageEmoticonParser extends MessageParser {

	public MessageEmoticonParser() {
		super();
	}

	@Override
	protected String getPattern() {
		return "(\\(\\p{Alnum}{1,15}\\))";
	}

	@Override
	protected JSONObject toJSONImpl(ParserResult parserResult, JSONObject jsonObject) throws JSONException {
		jsonObject.put("emoticons", new JSONArray(parserResult.getMatches()));
		return jsonObject;
	}

	@Override
	protected String cleanMatch(String match) {
		return match.substring(1, match.length() - 1);
	}

/*  An example of how I can add more logic into the parse loop
	@Override
	protected void onMatchFound(Matcher matcher, String match, ParserResult parserResult) {
		replaceMatches(matcher, match, parserResult, new MatchReplacer() {
			@Override
			public String replaceMatch(String match) {
				return "<" + match + ">";
			}
		});

		// More logic in here
	}
*/
}

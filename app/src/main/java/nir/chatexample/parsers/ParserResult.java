package nir.chatexample.parsers;

import java.util.List;

public class ParserResult {

	// All of the regex matched groups
	private List<String> matches;
	// StringBuffer and not StringBuilder since Matcher.appendReplacement work with StringBuffer
	private StringBuffer messageAfterReplacements;

	public ParserResult(List<String> matches, StringBuffer messageAfterReplacements) {
		this.matches = matches;
		this.messageAfterReplacements = messageAfterReplacements;
	}

	public ParserResult() {
		this(null, null);
	}

	public List<String> getMatches() {
		return matches;
	}

	public void setMatches(List<String> matches) {
		this.matches = matches;
	}

	public StringBuffer getMessageAfterReplacements() {
		return messageAfterReplacements;
	}

	public void setMessageAfterReplacements(StringBuffer messageAfterReplacements) {
		this.messageAfterReplacements = messageAfterReplacements;
	}
}

package nir.chatexample.parsers;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.regex.Pattern;

public class MessageLinkParser extends MessageParser {

	/****** The part below is copied from the latest Android SDK, we want to be updated in here *******/

	/**
	 * Good characters for Internationalized Resource Identifiers (IRI).
	 * This comprises most common used Unicode characters allowed in IRI
	 * as detailed in RFC 3987.
	 * Specifically, those two byte Unicode characters are not included.
	 */
	public static final String GOOD_IRI_CHAR =
			"a-zA-Z0-9\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF";
	public static final Pattern IP_ADDRESS
			= Pattern.compile(
			"((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
					+ "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
					+ "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
					+ "|[1-9][0-9]|[0-9]))");

	/**
	 * RFC 1035 Section 2.3.4 limits the labels to a maximum 63 octets.
	 */
	private static final String IRI
			= "[" + GOOD_IRI_CHAR + "]([" + GOOD_IRI_CHAR + "\\-]{0,61}[" + GOOD_IRI_CHAR + "]){0,1}";

	private static final String GOOD_GTLD_CHAR =
			"a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF";
	private static final String GTLD = "[" + GOOD_GTLD_CHAR + "]{2,63}";
	private static final String HOST_NAME = "(" + IRI + "\\.)+" + GTLD;
	public static final Pattern DOMAIN_NAME
			= Pattern.compile("(" + HOST_NAME + "|" + IP_ADDRESS + ")");

	/**
	 * Regular expression pattern to match most part of RFC 3987
	 * Internationalized URLs, aka IRIs.  Commonly used Unicode characters are
	 * added.
	 */
	public static final Pattern WEB_URL = Pattern.compile(
			"((?:(http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
					+ "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
					+ "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?"
					+ "(?:" + DOMAIN_NAME + ")"
					+ "(?:\\:\\d{1,5})?)" // plus option port number
					+ "(\\/(?:(?:[" + GOOD_IRI_CHAR + "\\;\\/\\?\\:\\@\\&\\=\\#\\~"  // plus option query params
					+ "\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?"
					+ "(?:\\b|$)"); // and finally, a word boundary or end of

	/****** The part above is copied from the latest Android SDK, we want to be updated in here *******/

	public MessageLinkParser() {
		super();
	}

	@Override
	protected String getPattern() {
		return null; // We will get the pattern directly from the android SDK
	}

	@Override
	protected JSONObject toJSONImpl(ParserResult parserResult, JSONObject jsonObject) throws Exception {
		JSONArray jsonArray = new JSONArray();

		for (String url : parserResult.getMatches()) {
			// Jsoup might be an overkill for just the title, but it give us a lot of flexibility and clear code
			JSONObject oneObject = new JSONObject();
			Document doc = Jsoup.connect(url).get();
			oneObject.put("url", url);
			// Title will be empty if not exists
			oneObject.put("title", doc.title());
			jsonArray.put(oneObject);
		}

		jsonObject.put("links", jsonArray);
		return jsonObject;
	}

	@Override
	protected Pattern compilePattern() {
		return WEB_URL;
	}

	/*  An example of how I can add more logic into the parse loop
	@Override
	protected void onMatchFound(Matcher matcher, String match, ParserResult parserResult) {
		replaceMatches(matcher, match, parserResult, new MatchReplacer() {
			@Override
			public String replaceMatch(String match) {
				return "<" + match + ">";
			}
		});

		// More logic in here
	}
	*/
}

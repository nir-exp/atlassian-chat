package nir.chatexample;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import nir.chatexample.parsers.MessageEmoticonParser;
import nir.chatexample.parsers.ParserResult;

import static org.junit.Assert.assertArrayEquals;

public class MessageEmoticonParserTest {

	private MessageEmoticonParser messageEmoticonParser;

	@Before
	public void setUp() throws Exception {
		messageEmoticonParser = new MessageEmoticonParser();
	}

	@Test
	public void testParse() {
		ParserResult result = messageEmoticonParser.parse("Good morning! (megusta) (coffee)");
		List<String> resultList = result.getMatches();
		assertArrayEquals(resultList.toArray(), new String[]{"megusta", "coffee"});

		result = messageEmoticonParser.parse("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016");
		resultList = result.getMatches();
		assertArrayEquals(new String[]{"success"}, resultList.toArray());

		result = messageEmoticonParser.parse("(one&two)((abc123)(d..dd)  \" $$ $2 (one more)   $1 (thisoneisgood)");
		resultList = result.getMatches();
		assertArrayEquals(new String[]{"abc123", "thisoneisgood"}, resultList.toArray());

		result = messageEmoticonParser.parse("(1231)");
		resultList = result.getMatches();
		assertArrayEquals(new String[]{"1231"}, resultList.toArray());
	}
}
package nir.chatexample;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import nir.chatexample.parsers.MessageMentionParser;
import nir.chatexample.parsers.ParserResult;

import static org.junit.Assert.assertArrayEquals;

public class MessageMentionParserTest {

	private MessageMentionParser messageMentionParser;

	@Before
	public void setUp() throws Exception {
		messageMentionParser = new MessageMentionParser();
	}

	@Test
	public void testParse() {
		ParserResult result = messageMentionParser.parse("@chris you around?");
		List<String> resultList = result.getMatches();
		assertArrayEquals(resultList.toArray(), new String[]{"chris"});

		result = messageMentionParser.parse("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016");
		resultList = result.getMatches();
		assertArrayEquals(new String[]{"bob", "john"}, resultList.toArray());

//		assertEquals("*@bob* *@john* (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016", result.getMessageAfterReplacements().toString());

		result = messageMentionParser.parse("@bob@john");
		resultList = result.getMatches();
		assertArrayEquals(new String[]{"bob", "john"}, resultList.toArray());

		result = messageMentionParser.parse("@bob.@john");
		resultList = result.getMatches();
		assertArrayEquals(new String[]{"bob", "john"}, resultList.toArray());

		// Without blocking it by running first an email parser, this will be count as a mention
		result = messageMentionParser.parse("this.is@an.email");
		resultList = result.getMatches();
		assertArrayEquals(new String[]{"an"}, resultList.toArray());

//		assertEquals("this.is*@an*.email", result.getMessageAfterReplacements().toString());
	}
}
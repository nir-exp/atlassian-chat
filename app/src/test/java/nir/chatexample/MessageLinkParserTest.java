package nir.chatexample;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import nir.chatexample.parsers.MessageLinkParser;
import nir.chatexample.parsers.ParserResult;

import static org.junit.Assert.assertArrayEquals;

public class MessageLinkParserTest {

	private MessageLinkParser messageLinkParser;

	@Before
	public void setUp() throws Exception {
		messageLinkParser = new MessageLinkParser();
	}

	@Test
	public void testParse() {
		ParserResult result = messageLinkParser.parse("Olympics are starting soon; http://www.nbcolympics.com");
		List<String> resultList = result.getMatches();
		assertArrayEquals(resultList.toArray(), new String[]{"http://www.nbcolympics.com"});

		result = messageLinkParser.parse("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016");
		resultList = result.getMatches();
		assertArrayEquals(new String[]{"https://twitter.com/jdorfman/status/430511497475670016"}, resultList.toArray());

		result = messageLinkParser.parse("https://twitter.com/jdorfman/status/430511497475670016 $ https://twitter.com/jdorfman/status");
		resultList = result.getMatches();
		assertArrayEquals(new String[]{"https://twitter.com/jdorfman/status/430511497475670016", "https://twitter.com/jdorfman/status"}, resultList.toArray());
//		assertEquals("<https://twitter.com/jdorfman/status/430511497475670016> $ <https://twitter.com/jdorfman/status>", result.getMessageAfterReplacements().toString());
	}
}